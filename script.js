let porcentagem = document.createElement('p')
let destino = document.getElementById('resposta')
let botao = document.getElementById('btn')
let container = document.querySelector('.container')
let bota_restart = document.getElementById('restart')
let final = document.querySelector('.final')
let pessoas = document.getElementById('respoderam')
let total = document.getElementById('totalPessoas')

botao.addEventListener('click', () => {
    let mult = 0
    let pessoas_value = document.getElementById('respoderam').value
    let total_value = document.getElementById('totalPessoas').value

    mult = (pessoas_value / total_value) * 100
    porcentagem.innerText = `${Math.round(mult)}%`
    destino.appendChild(porcentagem)

    container.style.display = 'none'
    bota_restart.style.display = 'initial'

    console.log('pessoas: ', pessoas)
})

bota_restart.addEventListener('click', () => {
    window.location.reload()
})

